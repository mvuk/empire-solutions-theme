<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<!-- BEGIN BASIC -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="completeFollowingAnd">Complete the following form and an Empire expert will contact you to schedule a customized consultation.</p>
        <?php echo do_shortcode( '[contact-form-4 id="115" title="Request Consultation"]' ); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->

<div class="container-fluid fixed-cta" id="fixedCta">
  <div class="row">
    <div class="col-md-12">
      <p><i class="fa fa-phone" aria-hidden="true"></i> <span><a class="fixed-tel-link" href="tel:1-844-756-3677">1-844-756-3677</a></span> <a class="drift-open-chat">Request a Consultation</a></p>
    </div>
  </div>
</div>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_html( $container ); ?>">

    <div class="row four">

      <div class="col-md-4 footerCol">

        <!--  -->

        <div class="row">
          <div class="col-md-12">
            <h3>Empire Solutions</h3>
          </div>
          <div class="col-md-6">
            <a href="/">Home</a>
            <a href="/about">About Us</a>
          </div>
          <div class="col-md-6">
            <a href="/partners">Partners</a>
            <a href="/contact">Contact</a>
          </div>
        </div>

        <!--  -->

      </div>

      <div class="col-md-6 footerCol">

        <div class="row">
          <div class="col-md-12">
            <h3>Our Services</h3>
          </div>
          <div class="col-md-6">
            <a href="/empire-storage">Empire Storage</a>
            <a href="/empire-connect">Empire Connect</a>
          </div>
          <div class="col-md-6">
            <a href="/professional-services">Professional Services</a>
            <a href="/empire-exchange">Empire Exchange</a>
          </div>
        </div>

      </div>

    </div>

		<div class="row displayNone">
			<div class="col-md-12">
				<footer class="site-footer" id="colophon">
					<div class="site-info">
						<p>Copyright 2017 Empire Solutions</p>
					</div><!-- .site-info -->
				</footer><!-- #colophon -->
			</div><!--col end -->
		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page -->

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-106200841-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- external JS, etc. -->
<script src="https://use.typekit.net/xae6fnw.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">


<!-- scroll js -->
<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

<script type="text/javascript">

console.log('scrollJs')
  // Make Footer CTA appear only on scroll
  window.sr = ScrollReveal();
  sr.reveal('#fixedCta', {distance: '150px'} );

</script>

<!-- DRIFT -->
<script>
!function() {
var t;
if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
t.factory = function(e) {
return function() {
var n;
return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
};
}, t.methods.forEach(function(e) {
t[e] = t.factory(e);
}), t.load = function(t) {
var e, n, o, i;
e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
});
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('6eiepefns87i');
</script>

<!-- Drift Btn Form  -->
<script>
(function() {
  var driftUserId = 120291;
  var $button = document.getElementById("schedule-meeting-button");
  $button.addEventListener("click", function() {
    drift.on("ready", function(api) {
      api.scheduleMeeting(driftUserId);
    });
  });
})();
</script>

<!-- place this script tag after the Drift embed tag -->
<script>
  (function() {
    /* Add this class to any elements you want to use to open Drift.
     *
     * Examples:
     * - <a class="drift-open-chat">Questions? We're here to help!</a>
     * - <button class="drift-open-chat">Chat now!</button>
     *
     * You can have any additional classes on those elements that you
     * would ilke.
     */
    var DRIFT_CHAT_SELECTOR = '.drift-open-chat'
    /* http://youmightnotneedjquery.com/#ready */
    function ready(fn) {
      if (document.readyState != 'loading') {
        fn();
      } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
      } else {
        document.attachEvent('onreadystatechange', function() {
          if (document.readyState != 'loading')
            fn();
        });
      }
    }
    /* http://youmightnotneedjquery.com/#each */
    function forEachElement(selector, fn) {
      var elements = document.querySelectorAll(selector);
      for (var i = 0; i < elements.length; i++)
        fn(elements[i], i);
    }
    function openSidebar(driftApi, event) {
      event.preventDefault();
      driftApi.sidebar.open();
      return false;
    }
    ready(function() {
      drift.on('ready', function(api) {
        var handleClick = openSidebar.bind(this, api)
        forEachElement(DRIFT_CHAT_SELECTOR, function(el) {
          el.addEventListener('click', handleClick);
        });
      });
    });
  })();
</script>


<!-- End drift btn -->

<!-- HIDE DRIFT -->
<script type="text/javascript">

if (page == "home") {
  console.log("home page, keep drift");
} else {
  console.log("not the home page, hide drift");
  drift.on('ready',function(api){
    // hide the widget when it first loads
  api.widget.hide()
   // show the widget when you receive a message
    drift.on('message',function(e){
      if(!e.data.sidebarOpen){
        api.widget.show()
      }
    })
    // hide the widget when you close the sidebar
      drift.on('sidebarClose',function(e){
        if(e.data.widgetVisible){
          api.widget.hide()
        }
      })
    })

}

</script>

<!-- end custom js -->

<?php wp_footer(); ?>

</body>

</html>
