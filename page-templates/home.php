<?php
/**
 * Template Name: Home
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- slider -->

<div class="container-fluid slider">
  <div class="row">
    <div class="col-lg-12 col-md-10 offset-lg-0 offset-md-1">
      <h1>Managed Information Technology In The Cloud</h1>
      <p>Re-define what it means to be IT. Empower your business with technology making you agile while you grow your empire to the next level.</p>
    </div>
  </div>
</div>

<!-- Add in a white div right here for "request a consultation" -->
<!-- <div class="container-flex flex-container homeTopCta" id="homeTopCta">
  <div class="row">
    <div class="col-md-6">
      <div class="rac-content">
        <h2>Request a Consultation</h2>
        <p>An Empire expert will contact you to schedule a custom consultation and demo.</p>
        <p>Give us a call at 1-800-000-0000</p>
      </div>
    </div>
    <div class="col-md-6">
      <div class="home-form">
        <?php echo do_shortcode( '[contact-form-7 id="115" title="Request Consultation"]' ); ?>
      </div>
    </div>
  </div>
</div> -->
<!-- request a consultation end -->

<div class="container-fluid flex-container product esc">
  <div class="row reverse empire-skype-connect">
    <div class="col-md-6">
      <div class="content">
        <h2>Empire Connect</h2>
        <p>Mobilize your workforce to work anywhere</p>
        <a href="/empire-connect">Learn More</a>
      </div>
    </div>
    <div class="col-md-6 img">
      <img src="/wp-content/uploads/2017/08/empire-skype-connect.png" alt="Empire Skype Connect">
    </div>
  </div>
</div>

<!-- content -->

<div class="container-fluid flex-container product ecs">
  <div class="row empire-cloud-storage">
    <div class="col-md-6">
      <div class="content">
        <h2>Empire Storage</h2>
        <p>Just use it. Never worry about free disk space.</p>
        <a href="/empire-storage">Learn More</a>
      </div>
    </div>
    <div class="col-md-6 img">
      <!-- <img src="/wp-content/uploads/2017/05/cloud.png" alt="Empire Cloud Storage"> -->
    </div>
  </div>
</div>

<!--  -->

<div class="container-fluid flex-container product egx">
  <div class="row reverse empire-global-connect">
    <div class="col-md-6">
      <div class="content">
        <h2>Empire Exchange</h2>
        <p>Connectivity in Canada, US, Europe and Asia.</p>
        <a href="/empire-exchange">Learn More</a>
      </div>
    </div>
    <div class="col-md-6 img">
      <img src="/wp-content/uploads/2017/09/empire-exchange-network-availability.jpg" alt="Empire Global Exchange">
    </div>
  </div>
</div>

<div class="container-fluid flex-container product ps">
  <div class="row professional-services">
    <div class="col-md-6">
      <div class="content">
        <h2>Professional Services</h2>
        <p>Let Empire implement effective strategies.</p>
        <a href="/professional-services">Learn More</a>
      </div>
    </div>
    <div class="col-md-6 img">
      <!-- <img src="/wp-content/uploads/2017/05/youngPros_sm.jpg" alt="Professional Services"> -->
    </div>
  </div>
</div>

<div class="container-fluid partners">
  <div class="row">
    <div class="col-md-12">
      <h2>Empire Technology Partners</h2>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img aws"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img hpe"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img ms"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img cisco"></div></div>
  </div>
  <div class="row join-network-of">
    <div class="col-md-12">
      <a href="/partners">Join our network of partners</a>
    </div>
  </div>
</div>

<div class="container-fluid flex-container contactFormCon" id="ReqCon">
  <div class="row">

    <div class="col-md-6 letsTalkNow">
      <div class="contactWrapper">
        <h3><i class="fa fa-phone" aria-hidden="true"></i> Let's Talk</h3>
        <p class="callNum">Call <a href="tel:1-844-756-3677" class="hiddentel formhiddentel">+1 844-756-3677</a> or click to dial</p>
        <?php echo do_shortcode("[wpc2c label='Click To Call' number='+18447563677']"); ?>
        <div class="poweredby">
          <p><i class="fa fa-star" aria-hidden="true"></i> Powered by Empire Connect</p>
        </div>
        <!-- <div class="row phoneform">
          <div class="col-md-6">
            <input type="tel" class="form-control" placeholder="Your Number Here" id="PhoneNumber">
          </div>
          <div class="col-md-4">
            <button type="button" name="button" class="btn btn-primary" id="CallMe">Call Me</button>
          </div>
        </div> -->
      </div>
    </div>

    <div class="col-md-6 bookMeeting">
      <div class="contactWrapper">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Can't Talk Now?</h3>
        <p class="callNum">Lets set a time for a call</p>
        <div class="schedBtn">
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-primary" id="schedule-meeting-button">Schedule Meeting</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- end content -->

<script type="text/javascript">
  var page ="home";
</script>

<?php get_footer(); ?>
