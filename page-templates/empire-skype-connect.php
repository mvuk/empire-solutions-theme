<?php
/**
 * Template Name: Empire Skype Connect
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<div class="container-fluid slider solutionsSlider">
    <div class="row">
      <div class="col-md-12">
        <h1>Empire Connect</h1>
        <p>The most secure way to mobilize your business.</p>
      </div>
    </div>
</div>

<div class="container-fluid flex-container opening-section">
  <div class="row">
    <!-- <div class="col-md-6 img">
      <img src="/wp-content/uploads/2017/06/cross-platform.png" alt="Cross Platform" class="cross-platform">
    </div> -->
    <div class="col-md-10 offset-md-1 content">
      <div class="poweredByImg">
        <img src="/wp-content/uploads/2017/09/power-by-ms.png" alt="Powered by Microsoft" class="ms">
      </div>
      <p>Empire Connect allows for complete mobility of your workforce.</p>
      <ul>
        <p>Collaborate with anyone, anywhere on any device</p>
        <p>Keep your number and your carrier</p>
        <p>Backed up, and always available in the cloud</p>
        <p>Seamlessly integrate without downtime</p>
      </ul>
    </div>
  </div>
</div>

<!-- Full width -->

<div class="container-fluid flex-container reasons">
  <div class="row">
    <!-- <div class="col-md-12 content content">
      <h2>Why do so many trust Empire Skype Connect?</h2>
      <ul>
        <p>Powered with Microsoft Office 365</p>
        <p>Comprehensive support and customer service</p>
        <p>Operates with your carrier and phone numbers</p>
        <p>Zero impact migration</p>
      </ul>
    </div> -->
    <div class="col-md-6 content">
      <h2>How it works</h2>
      <ol>
        <li>Connect your mobile phone to your business by installing an app.</li>
        <li>Install our secure gateway at your office or connect to our cloud gateway.</li>
        <li>Calls from the office or a mobile device use the gateway to reach anyone, anywhere.</li>
      </ol>
    </div>
    <div class="col-md-6">
      <img src="/wp-content/uploads/2017/08/empire-drawing-1.png" alt="">
    </div>

  </div>
</div>

<div class="container-fluid flex-container contactFormCon" id="ReqCon">
  <div class="row">
    <div class="col-md-12 plans">
      <p class="primary">Empire Solutions offers different plans based on your requirements.</p>
      <p class="secondary">Explore with us support options and choose the plan that best fits, whether you're a start-up just beginning your journey or a large organization deploying business-critical, strategic applications Empire Solutions has you covered.</p>
    </div>
  </div>
  <div class="row">

    <div class="col-md-6 letsTalkNow">
      <div class="contactWrapper">
        <h3><i class="fa fa-phone" aria-hidden="true"></i> Let's Talk</h3>
        <p class="callNum">Call <a href="tel:1-844-756-3677" class="hiddentel formhiddentel">+1 844-756-3677</a> or click to dial</p>
        <?php echo do_shortcode("[wpc2c label='Click To Call' number='+01234567892']"); ?>
        <div class="poweredby">
          <p><i class="fa fa-star" aria-hidden="true"></i> Powered by Empire Connect</p>
        </div>
        <!-- <div class="row phoneform">
          <div class="col-md-6">
            <input type="tel" class="form-control" placeholder="Your Number Here" id="PhoneNumber">
          </div>
          <div class="col-md-4">
            <button type="button" name="button" class="btn btn-primary" id="CallMe">Call Me</button>
          </div>
        </div> -->
      </div>
    </div>

    <div class="col-md-6 bookMeeting">
      <div class="contactWrapper">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Can't Talk Now?</h3>
        <p class="callNum">Lets set a time for a call</p>
        <div class="schedBtn">
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-primary" id="schedule-meeting-button">Schedule Meeting</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div class="container-fluid price-table-container" id="priceTable">
  <div class="row">
    <div class="col-md-12">
      <h3>Empire Connect Plans</h3>
    </div>
    <div class="col-md-4 price-table">
      <div class="content">
        <h3>Basic</h3>
        <ul>
          <p>Empire Connect Gateway</p>
          <p>Email Support</p>
          <p>Self Service Portal</p>
        </ul>
        <a href="#priceTable" class="requestQuote" data-toggle="modal" data-target="#BasicModal">Request Quote</a>
      </div>
    </div>
    <div class="col-md-4 price-table">
      <div class="content">
        <h3>Standard</h3>
        <ul>
          <p>Empire Connect Gateway</p>
          <p>Email and Phone Support</p>
          <p>Self Service Portal</p>
          <p>Professional Service Installation</p>
          <p>Daily cloud backups</p>
        </ul>
        <a href="#priceTable" class="requestQuote" data-toggle="modal" data-target="#StandardModal">Request Quote</a>
      </div>
    </div>
    <div class="col-md-4 price-table">
      <div class="content">
        <h3>Enterprise</h3>
        <ul>
          <p>Empire Connect Gateway</p>
          <p>24/7 Support</p>
          <p>Self Service Portal</p>
          <p>Professional Service Installation</p>
          <p>4 Hour Recovery and hardware replacement</p>
        </ul>
        <a href="#priceTable" class="requestQuote" data-toggle="modal" data-target="#EnterpriseModal">Request Quote</a>
      </div>
    </div>
  </div>
</div>

<!-- MODAL WINDOWS -->

<!-- BEGIN MODAL -->
<div class="modal fade" id="BasicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalForm" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <?php echo do_shortcode('[gravityforms id="4" field_values="product=EmpireConnect&amp;option=Basic"]'); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->
<!-- BEGIN MODAL -->
<div class="modal fade" id="StandardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalForm" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <?php echo do_shortcode('[gravityforms id="4" field_values="product=EmpireConnect&amp;option=Standard"]'); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->
<!-- BEGIN MODAL -->
<div class="modal fade" id="EnterpriseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalForm" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <?php echo do_shortcode('[gravityforms id="4" field_values="product=EmpireConnect&amp;option=Enterprise"]'); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->


<!-- end content -->

<?php get_footer(); ?>
