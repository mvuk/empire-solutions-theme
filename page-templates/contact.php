<?php
/**
 * Template Name: Contact
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<div class="container-fluid slider">
    <div class="row">
      <div class="col-md-12">
        <h1>Contact Us</h1>
      </div>
    </div>
</div>

<div class="container simpleEmail">
  <div class="row">
    <div class="col-md-6 email">
      <p><i class="fa fa-envelope" aria-hidden="true"></i> Email Us: <span>contact@empiresolutions.ca</span></p>
    </div>
  </div>
</div>

<div class="container-fluid flex-container contactFormCon" id="ReqCon">
  <div class="row">

    <div class="col-md-6 letsTalkNow">
      <div class="contactWrapper">
        <h3><i class="fa fa-phone" aria-hidden="true"></i> Let's Talk</h3>
        <p class="callNum">Call <a href="tel:1-844-756-3677" class="hiddentel formhiddentel">+1 844-756-3677</a> or click to dial</p>
        <?php echo do_shortcode("[wpc2c label='Click To Call' number='+01234567892']"); ?>
        <div class="poweredby">
          <p><i class="fa fa-star" aria-hidden="true"></i> Powered by Empire Connect</p>
        </div>
        <!-- <div class="row phoneform">
          <div class="col-md-6">
            <input type="tel" class="form-control" placeholder="Your Number Here" id="PhoneNumber">
          </div>
          <div class="col-md-4">
            <button type="button" name="button" class="btn btn-primary" id="CallMe">Call Me</button>
          </div>
        </div> -->
      </div>
    </div>

    <div class="col-md-6 bookMeeting">
      <div class="contactWrapper">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Can't Talk Now?</h3>
        <p class="callNum">Lets set a time for a call</p>
        <div class="schedBtn">
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-primary" id="schedule-meeting-button">Schedule Meeting</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="container-fluid contact">
  <div class="row cards">
    <div class="col-md-6 col-lg-4 location">
      <div class="card">
        <div class="image to">
          <h3>Toronto</h3>
        </div>
        <div class="content">
          <p>88 Queens Quay W, Suite 2500</p>
          <p>Toronto, ON M5J 0B8</p>
          <a class="hiddentel" href="tel:1-647-691-3466">+1 647-691-3466</a>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4 location">
      <div class="card">
        <div class="image pa">
          <h3>Palo Alto</h3>
        </div>
        <div class="content">
          <p>3000 El Camino Real, Suite 200</p>
          <p>Palo Alto, CA 94306</p>
          <a class="hiddentel" href="tel:1-650-665-5220">+1 650-665-5220</a>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 location">
      <div class="card">
        <div class="image ny">
          <h3>New York</h3>
        </div>
        <div class="content">
          <p>200 Vesey Street, Suite 2400</p>
          <p>New York City, NY 10281</p>
          <a class="hiddentel" href="tel:1-917-720-5649">+1 917-720-5649</a>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- end content -->

<?php get_footer(); ?>
