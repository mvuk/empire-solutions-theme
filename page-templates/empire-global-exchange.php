<?php
/**
 * Template Name: Empire Global Exchange
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<!-- slider -->

<div class="container-fluid slider solutionsSlider">
    <div class="row">
      <div class="col-md-12">
        <h1>Empire Exchange</h1>
        <p>The most cost effective way to exchange global networks securely.</p>
      </div>
    </div>
</div>

<div class="container-fluid flex-container opening-section">
  <div class="row">
    <!-- <div class="col-md-6 img">
      <img src="/wp-content/uploads/2017/06/cross-platform.png" alt="Cross Platform" class="cross-platform">
    </div> -->
    <div class="col-md-10 offset-md-1 content">
      <div class="poweredByImg">
        <img src="/wp-content/uploads/2017/09/powered-by-cisco.png" alt="Powered by Cisco" class="cisco">
      </div>
      <p>Empire Global Exchange provides a low cost, low risk Ethernet switch environment that allows you to interconnect to any carrier or customer within our multiple points of presence across the world. Quickly and cost effectively connect through fiber cross connects to any network with speeds from 200 Mbps to 10 Gbps.</p>
    </div>
  </div>
</div>

<!-- Full width -->

<div class="container-fluid flex-container reasons">
  <div class="row">
    <div class="col-md-12 content">
      <h2>Why use Empire Global Exchange?</h2>
      <ul>
        <p>Private and secure connection between your networks</p>
        <p>200 Mbps to 10 Gbps performance bandwidth</p>
        <p>Reduce operational costs</p>
        <p>Carrier agnostic design</p>
      </ul>
    </div>
  </div>
</div>

<div class="container-fluid networkMap">
    <div class="row row-fluid">
      <div class="col-md-12">
        <h2>Network Availability</h2>
      </div>
      <!--  -->
      <div class="col-md-12 center">
        <img class="mapImg" src="/wp-content/uploads/2017/09/empire-exchange-network-availability.jpg" alt="Empire Exchange Map">
      </div>
      <!--  -->
      <div class="col-md-8 offset-md-2">
        <!--  -->
        <table class="table">
          <thead class="thead-default">
            <tr>
              <th>Country</th>
              <th>City</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <!--  -->
            <tr>
              <!-- <td><strong>Australia</strong></td>
              <td>Sydney</td>
              <td><span class="unav">Coming Soon</span></td> -->
            </tr>
            <tr>
              <td><strong>Canada</strong></td>
              <td>Montreal</td>
              <td><span class="available">Available</span></td>
            </tr>
            <tr>
              <td></td>
              <td>Toronto</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!--  -->
            <tr>
              <td><strong>France</strong></td>
              <td>Lyon</td>
              <td><span class="available">Available</span></td>
            </tr>
            <tr>
              <td></td>
              <td>Paris</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!-- <tr>
              <td><strong>Germany</strong></td>
              <td>Frankfurt</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <!--  -->
            <!-- <tr>
              <td><strong>Italy</strong></td>
              <td>Milan</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <tr>
              <td><strong>Netherlands</strong></td>
              <td>Amsterdam</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!-- <tr>
              <td><strong>Poland</strong></td>
              <td>Warsaw</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <!--  -->
            <!-- <tr>
              <td><strong>Singapore</strong></td>
              <td>Singapore</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr>
            <tr>
              <td><strong>Spain</strong></td>
              <td>Madrid</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <tr>
              <td><strong>United Kingdom</strong></td>
              <td>London</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!--  -->
            <!-- <tr>
              <td><strong>US East Coast</strong></td>
              <td>Ashburn</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <tr>
              <td></td>
              <td>Chicago</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!-- <tr>
              <td></td>
              <td>Dallas</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr>
            <tr>
              <td></td>
              <td>Miami</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <!-- <tr>
              <td></td>
              <td>Newark</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <tr>
              <td></td>
              <td>New-York</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!--  -->
            <tr>
              <td><strong>US West Coast</strong></td>
              <td>Los Angeles</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!-- <tr>
              <td></td>
              <td>San Jose</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <tr>
              <td></td>
              <td>Palo Alto</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!-- <tr>
              <td></td>
              <td>Seattle</td>
              <td><span class="unav">Coming Soon</span></td>
            </tr> -->
            <!--  -->
            <tr>
              <td><strong>China</strong></td>
              <td>Hong Kong</td>
              <td><span class="available">Available</span></td>
            </tr>
            <tr>
              <td><strong>Japan</strong></td>
              <td>Tokyo</td>
              <td><span class="available">Available</span></td>
            </tr>
            <!--  -->
          </tbody>
        </table>

        <!--  -->
      </div>

    </div>
</div>

<div class="container-fluid flex-container contactFormCon" id="ReqCon">
  <div class="row">

    <div class="col-md-6 letsTalkNow">
      <div class="contactWrapper">
        <h3><i class="fa fa-phone" aria-hidden="true"></i> Let's Talk</h3>
        <p class="callNum">Call <a href="tel:1-844-756-3677" class="hiddentel formhiddentel">+1 844-756-3677</a> or click to dial</p>
        <?php echo do_shortcode("[wpc2c label='Click To Call' number='+01234567892']"); ?>
        <div class="poweredby">
          <p><i class="fa fa-star" aria-hidden="true"></i> Powered by Empire Connect</p>
        </div>
        <!-- <div class="row phoneform">
          <div class="col-md-6">
            <input type="tel" class="form-control" placeholder="Your Number Here" id="PhoneNumber">
          </div>
          <div class="col-md-4">
            <button type="button" name="button" class="btn btn-primary" id="CallMe">Call Me</button>
          </div>
        </div> -->
      </div>
    </div>

    <div class="col-md-6 bookMeeting">
      <div class="contactWrapper">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Can't Talk Now?</h3>
        <p class="callNum">Lets set a time for a call</p>
        <div class="schedBtn">
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-primary" id="schedule-meeting-button">Schedule Meeting</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- end content -->

<?php get_footer(); ?>
