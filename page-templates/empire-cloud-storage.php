<?php
/**
 * Template Name: Empire Cloud Storage
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<!-- slider -->

<div class="container-fluid slider solutionsSlider">
    <div class="row">
      <div class="col-md-12">
        <h1>Empire Storage</h1>
        <p>The most secure way to share infinite files with all your offices</p>
      </div>
    </div>
</div>

<div class="container-fluid flex-container opening-section">
  <div class="row">
    <!-- <div class="col-md-6 img">
      <img src="/wp-content/uploads/2017/06/cross-platform.png" alt="Cross Platform" class="cross-platform">
    </div> -->
    <div class="col-md-10 offset-md-1 content">
      <div class="poweredByImg">
        <img src="/wp-content/uploads/2017/09/powered-by-aws.png" alt="Powered by Amazon Web Services" class="aws">
      </div>
      <!-- <p>Empire Storage goes beyond local storage and connects your business data with the endless capacity of the cloud. Engineered by Amazon Web Services, the industry leader in scalable cloud computing, Empire Storage provides the most cost-effective storage solution in the market today.</p> -->
      <p>Empire Storage gives you all the flexibility of the cloud and brings it to your office.</p>
      <ul>
        <p>Securely access files anywhere anytime from any office</p>
        <p>Unllimited storage with the power of the cloud</p>
        <p>Backed up, and always available</p>
        <p>Seamlessly integrate without downtime</p>
      </ul>
    </div>
  </div>
</div>

<!-- Full width -->

<div class="container-fluid flex-container reasons">
  <div class="row">
    <div class="col-md-6 content">
      <h2>How it works</h2>
      <ol>
        <li>Connect a file share to our storage gateway.</li>
        <li>Active files are cached locally while infrequently used files are sent to the cloud.</li>
        <li>When a file is needed but not cached, it's pulled down from the cloud.</li>
        <li>When using multiple storage gateways, data is replicated automatically to each location.</li>
      </ol>
    </div>
    <div class="col-md-6">
      <img src="/wp-content/uploads/2017/08/empire-storage.png" alt="Empire Storage">
    </div>
  </div>
</div>

<!-- <div class="container-fluid flex-container conMoreExp">
  <div class="row">
    <div class="col-md-10 offset-md-1 more-explain">
      <p>The most secure way to connect existing storage applications and workflows to regional offices around the world. Empire storage provides the capacity to reach your data anywhere in real-time.</p>
      <ul>
        <li>Securely sync data automatically between offices and datacentres</li>
        <li>Effortlessly backup and restore your data in minutes</li>
        <li>Instantly access files after changes globally</li>
      </ul>
    </div>
  </div>
</div> -->

<!-- <div class="container-fluid flex-container howitworks">
  <div class="row">
    <div class="col-md-8 offset-md-2">
      <img src="/wp-content/uploads/2017/06/empire-cloud-storage-diagrams.jpg" alt="diagram" class="diagram">
    </div>
  </div>
</div> -->

<div class="container-fluid flex-container contactFormCon" id="ReqCon">

  <div class="row">
    <div class="col-md-12 plans">
      <p class="primary">Empire Solutions offers different plans based on your requirements.</p>
      <p class="secondary">Explore with us support options and choose the plan that best fits, whether you're a start-up just beginning your journey or a large organization deploying business-critical, strategic applications Empire Solutions has you covered.</p>
    </div>
  </div>

  <div class="row">

    <div class="col-md-6 letsTalkNow">
      <div class="contactWrapper">
        <h3><i class="fa fa-phone" aria-hidden="true"></i> Let's Talk</h3>
        <p class="callNum">Call <a href="tel:1-844-756-3677" class="hiddentel formhiddentel">+1 844-756-3677</a> or click to dial</p>
        <?php echo do_shortcode("[wpc2c label='Click To Call' number='+01234567892']"); ?>
        <div class="poweredby">
          <p><i class="fa fa-star" aria-hidden="true"></i> Powered by Empire Connect</p>
        </div>
        <!-- <div class="row phoneform">
          <div class="col-md-6">
            <input type="tel" class="form-control" placeholder="Your Number Here" id="PhoneNumber">
          </div>
          <div class="col-md-4">
            <button type="button" name="button" class="btn btn-primary" id="CallMe">Call Me</button>
          </div>
        </div> -->
      </div>
    </div>

    <div class="col-md-6 bookMeeting">
      <div class="contactWrapper">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Can't Talk Now?</h3>
        <p class="callNum">Lets set a time for a call</p>
        <div class="schedBtn">
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-primary" id="schedule-meeting-button">Schedule Meeting</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="container-fluid price-table-container" id="priceTable">
  <div class="row">
    <div class="col-md-12">
      <h3>Empire Storage Plans</h3>
    </div>
    <div class="col-md-4 price-table">
      <div class="content">
        <h3>Basic</h3>
        <!-- <h4>$0.032 per GB</h4> -->
        <ul>
          <p>Empire Storage Gateway</p>
          <p>Email Support</p>
          <p>Self Service Portal</p>
        </ul>
        <a href="#priceTable" class="requestQuote" data-toggle="modal" data-target="#BasicModal">Request Quote</a>
      </div>
    </div>
    <div class="col-md-4 price-table">
      <div class="content">
        <h3>Standard</h3>
        <!-- <h4>$0.028 per GB*</h4> -->
        <ul>
          <p>Empire Storage Gateway</p>
          <p>Email and Phone Support</p>
          <p>Self Service Portal</p>
          <p>Professional Service Installation</p>
          <p>Daily Cloud Backups</p>
        </ul>
        <a href="#priceTable" class="requestQuote" data-toggle="modal" data-target="#StandardModal">Request Quote</a>
      </div>
    </div>
    <div class="col-md-4 price-table">
      <div class="content">
        <h3>Enterprise</h3>
        <!-- <h4>$0.025 per GB*</h4> -->
        <ul>
          <p>Empire Storage Gateway</p>
          <p>24/7 Support</p>
          <p>Self Service Portal</p>
          <p>Professional Service Installation</p>
          <p>4 Hour Recovery and hardware replacement</p>
        </ul>
        <a href="#priceTable" class="requestQuote" data-toggle="modal" data-target="#EnterpriseModal">Request Quote</a>
      </div>
    </div>
  </div>
</div>

<!-- BEGIN MODAL -->
<div class="modal fade" id="BasicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalForm" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <?php echo do_shortcode('[gravityforms id="4" field_values="product=EmpireStorage&amp;option=Basic"]'); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->
<!-- BEGIN MODAL -->
<div class="modal fade" id="StandardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalForm" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <?php echo do_shortcode('[gravityforms id="4" field_values="product=EmpireStorage&amp;option=Standard"]'); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->
<!-- BEGIN MODAL -->
<div class="modal fade" id="EnterpriseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modalForm" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Request a Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <?php echo do_shortcode('[gravityforms id="4" field_values="product=EmpireStorage&amp;option=Enterprise"]'); ?>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->



<!-- end content -->

<?php get_footer(); ?>
