<?php
/**
 * Template Name: Professional Services
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<!-- slider -->

<div class="container-fluid slider solutionsSlider">
    <div class="row">
      <div class="col-md-12">
        <h1>Professional Services</h1>
        <p>Let Empire Solutions implement strategies effectively turning process into results.</p>
      </div>
    </div>
</div>

<div class="container-fluid flex-container opening-section">
  <div class="row">
    <!-- <div class="col-md-6 img">
      <img src="/wp-content/uploads/2017/06/cross-platform.png" alt="Cross Platform" class="cross-platform">
    </div> -->
    <div class="col-md-10 offset-md-1 content">
      <p>Empire Solutions is a IT consulting firm, focused on partnering with leaders to tackle problems, facilitate change, and deliver solutions that drive effective IT. Empire delivers results and saves you time.</p>
    </div>
  </div>
</div>

<!-- Full width -->

<div class="container-fluid flex-container reasons">
  <div class="row">
    <div class="col-md-12 content content">
      <div class="row">
        <div class="col-md-12 ps-title">
          <h2>Our Expertise</h2>
        </div>
        <div class="col-md-6">
          <ul>
            <p>Strategy</p>
            <!-- <p>Process Improvement</p> -->
            <!-- <p>Project Management</p> -->
            <!-- <p>Research and Analytics</p> -->
            <p>Procurement Cost Reduction</p>
          </ul>
        </div>
        <div class="col-md-6">
          <ul>
            <p>Network Design and Architecture</p>
            <p>RFP Management</p>
            <p>Systems Integrations</p>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="container-fluid flex-container contactFormCon" id="ReqCon">

  <div class="row">

    <div class="col-md-6 letsTalkNow">
      <div class="contactWrapper">
        <h3><i class="fa fa-phone" aria-hidden="true"></i> Let's Talk</h3>
        <p class="callNum">Call <a href="tel:1-844-756-3677" class="hiddentel formhiddentel">+1 844-756-3677</a> or click to dial</p>
        <?php echo do_shortcode("[wpc2c label='Click To Call' number='+01234567892']"); ?>
        <div class="poweredby">
          <p><i class="fa fa-star" aria-hidden="true"></i> Powered by Empire Connect</p>
        </div>
        <!-- <div class="row phoneform">
          <div class="col-md-6">
            <input type="tel" class="form-control" placeholder="Your Number Here" id="PhoneNumber">
          </div>
          <div class="col-md-4">
            <button type="button" name="button" class="btn btn-primary" id="CallMe">Call Me</button>
          </div>
        </div> -->
      </div>
    </div>

    <div class="col-md-6 bookMeeting">
      <div class="contactWrapper">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Can't Talk Now?</h3>
        <p class="callNum">Lets set a time for a call</p>
        <div class="schedBtn">
          <div class="col-md-6">
            <button type="button" name="button" class="btn btn-primary" id="schedule-meeting-button">Schedule Meeting</button>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- end content -->

<?php get_footer(); ?>
