<?php
/**
 * Template Name: Partners
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<div class="container-fluid slider">
    <div class="row">
      <div class="col-md-12">
        <h1>Gain Experience and Connect with Clients</h1>
      </div>
    </div>
</div>

<div class="container-fluid partners">
  <div class="row">
    <div class="col-md-12">
      <h2>Our Partners</h2>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img aws"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img hpe"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img ms"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img cisco"></div></div>
  </div>
  <!-- <div class="row join-network-of">
    <div class="col-md-12">
      <a href="/partners">Join our network of partners</a>
    </div>
  </div> -->
</div>

<!--  -->
<div class="container-fluid flex-container reasons partnerReasons">
  <div class="row">
    <div class="col-md-12 content">
      <h2>Join our Partner Network</h2>
      <ul>
        <p>Work with cutting edge technology</p>
        <p>Expand your business presence</p>
        <p>Build substantial return on your investment</p>
        <p>Take advantage of our internal resources at your disposal</p>
      </ul>
    </div>
  </div>
</div>
<!--  -->

<div class="container-fluid flex-container form">
  <div class="row">

    <div class="col-md-6 offset-md-2 partnerForm">
      <div class="row">

        <div class="col-md-12">
          <div class="partnerFormIntro">
            <h2>Join the Empire</h2>
            <h3>Partnership Consultation Form</h3>
            <?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>

<!--  -->

<!-- end content -->

<?php get_footer(); ?>
