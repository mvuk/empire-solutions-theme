<?php
/**
 * Template Name: About
 *
 * This is the template from which page templates should be copied from.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- content -->

<div class="container-fluid slider">
    <div class="row">
      <div class="col-md-12">
        <h1>About the Empire</h1>
      </div>
    </div>
</div>

<!--  -->
<div class="container-fluid flex-container opening-section">
  <div class="row">
    <div class="col-md-11 content">
      <p>Empire Solutions offers a vast range of IT services for businesses and tech start-ups. From system integrations to voice and storage solutions, all our services benefit from continued development and are regularly improved with new technologies.</p>
      <p>Empire is a Toronto-based IT management and consulting firm that provides services customized to address the growing gap between technology and business needs. Focused on integration and innovation, Empire Solutions takes an active role in leading companies across North America to the next level.</p>
    </div>
  </div>
</div>
<!--  -->


<div class="container-fluid industries">
  <div class="row row-fluid title">
    <div class="col-md-12">
      <h2>Industries Served</h2>
    </div>
  </div>
  <div class="row row-fluid industry">

    <!-- <div class="col-md-4 col-sm-6">
      <div class="sample-industry a">
        <div class="img">
          <h3>Hospitality</h3>
        </div>
        <div class="description">
          <p>Reduce operational expenses across communication and storage.</p>
        </div>
      </div>
    </div> -->

    <div class="col-md-4 col-sm-6">
      <div class="sample-industry b">
        <div class="img">
          <h3>Architecture</h3>
        </div>
        <div class="description">
          <!-- <p>Reduce delay and share large CAD files instantly across offices and manufacturing plants.</p> -->
          <p>Reduce delay and share CAD drawings instantly across regional offices.</p>
        </div>
      </div>
    </div>

    <!-- <div class="col-md-4 col-sm-6">
      <div class="sample-industry c">
        <div class="img">
          <h3>Retail</h3>
        </div>
        <div class="description">
          <p>Embracing technology to improve efficiency and market reach is how to survive in a global market.</p>
        </div>
      </div>
    </div> -->

    <div class="col-md-4 col-sm-6">
      <div class="sample-industry d">
        <div class="img">
          <h3>Finance</h3>
        </div>
        <div class="description">
          <!-- <p>Distributed offices requires a sophisticated understanding of how to securely communicate and control access to private data.</p> -->
          <p>Your enterprise requires a partner who can deliver on security and control to private data.</p>
        </div>
      </div>
    </div>

    <!-- <div class="col-md-4 col-sm-6">
      <div class="sample-industry e">
        <div class="img">
          <h3>Automotive</h3>
        </div>
        <div class="description">
          <p>Embracing technology to improve efficiency and market reach is how to survive in a global market.</p>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-6">
      <div class="sample-industry f">
        <div class="img">
          <h3>Education</h3>
        </div>
        <div class="description">
          <p>Distributed offices requires a sophisticated understanding of how to securely communicate and control access to private data.</p>
        </div>
      </div>
    </div> -->

    <!-- <div class="col-md-4 col-sm-6">
      <div class="sample-industry g">
        <div class="img">
          <h3>Culture & Arts</h3>
        </div>
        <div class="description">
          <p>Embracing technology to improve efficiency and market reach is how to survive in a global market.</p>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-6">
      <div class="sample-industry h">
        <div class="img">
          <h3>Agriculture</h3>
        </div>
        <div class="description">
          <p>Distributed offices requires a sophisticated understanding of how to securely communicate and control access to private data.</p>
        </div>
      </div>
    </div> -->

    <div class="col-md-4 col-sm-6">
      <div class="sample-industry i">
        <div class="img">
          <h3>Health Care</h3>
        </div>
        <div class="description">
          <!-- <p>Embracing technology to improve efficiency and market reach is how to survive in a global market.</p> -->
          <p>Embracing technology improves healthcare workloads through the digitization of information.</p>
        </div>
      </div>
    </div>

    <!-- <div class="col-md-4 col-sm-6">
      <div class="sample-industry j">
        <div class="img">
          <h3>Real Estate</h3>
        </div>
        <div class="description">
          <p>Distributed offices requires a sophisticated understanding of how to securely communicate and control access to private data.</p>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-6">
      <div class="sample-industry k">
        <div class="img">
          <h3>Logistics & Transportation</h3>
        </div>
        <div class="description">
          <p>Embracing technology to improve efficiency and market reach is how to survive in a global market.</p>
        </div>
      </div>
    </div> -->

    <!-- <div class="col-md-4 col-sm-6">
      <div class="sample-industry l">
        <div class="img">
          <h3>Construction</h3>
        </div>
        <div class="description">
          <p>Distributed offices requires a sophisticated understanding of how to securely communicate and control access to private data.</p>
        </div>
      </div>
    </div> -->

  </div>
</div>

<div class="container-fluid partners">
  <div class="row">
    <div class="col-md-12">
      <h2>Empire Technology Partners</h2>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img aws"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img hpe"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img ms"></div></div>
    <div class="col-md-3 col-sm-6 col-xs-6"><div class="img cisco"></div></div>
  </div>
  <div class="row join-network-of">
    <div class="col-md-12">
      <a href="/partners">Join our network of partners</a>
    </div>
  </div>
</div>

<div class="container-fluid contact">
  <div class="row">
    <div class="col-md-12">
      <h2>Our Locations</h3>
    </div>
  </div>
  <div class="row cards">
    <div class="col-md-6 col-lg-4 location">
      <div class="card">
        <div class="image to">
          <h3>Toronto</h3>
        </div>
        <div class="content">
          <p>88 Queens Quay W, Suite 2500</p>
          <p>Toronto, ON M5J 0B8</p>
          <a class="hiddentel" href="tel:1-647-691-3466">+1 647-691-3466</a>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4 location">
      <div class="card">
        <div class="image pa">
          <h3>Palo Alto</h3>
        </div>
        <div class="content">
          <p>3000 El Camino Real, Suite 200</p>
          <p>Palo Alto, CA 94306</p>
          <a class="hiddentel" href="tel:1-650-665-5220">+1 650-665-5220</a>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-4 location">
      <div class="card">
        <div class="image ny">
          <h3>New York</h3>
        </div>
        <div class="content">
          <p>200 Vesey Street, Suite 2400</p>
          <p>New York City, NY 10281</p>
          <a class="hiddentel" href="tel:1-917-720-5649">+1 917-720-5649</a>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- end content -->

<script type="text/javascript">
  drift.on('ready',function(api){
  // hide the widget when it first loads
  api.widget.hide()
</script>

<?php get_footer(); ?>
