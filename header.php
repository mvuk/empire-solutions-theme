<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div class="wrapper-fluid wrapper-navbar enav" id="wrapper-navbar">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
		'understrap' ); ?></a>

		<nav id="es-nav" class="navbar navbar-toggleable-md es-nav">

		<?php if ( 'container' == $container ) : ?>
			<div class="container nav-container">
		<?php endif; ?>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

					<!-- menu items -->
					<!-- <a class="shiftnav-toggle shiftnav-toggle-button" data-shiftnav-target="shiftnav-main"><i class="fa fa-bars"></i> Menu</a> -->
					<ul class="nav navbar-nav navbar-right" id="navbar-right">
						<li><a href="#" id="solutions" onclick="openTheMenu()">Solutions</a></li>
						<li><a href="/about">About</a></li>
						<li><a href="/partners">Partners</a></li>
						<li><a href="/contact">Contact</a></li>
					</ul>

			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->

			<!-- START MENU CONTAINER -->

			<div class="container nav-menu-area inactive" id="nav-menu-area">

							<div class="row">

										<div class="col-md-6 solution-quarter">
											<a href="/empire-storage">
												<div class="solution-div">
													<div class="quarter-img ecs"></div>
													<!-- <img src="/wp-content/uploads/2017/08/empire-cloud-storage.jpg" alt="Solutions"> -->
													<h3>Empire Storage</h3>
													<p>Never run out of storage again.</p>
												</div>
											</a>
										</div>

										<div class="col-md-6 solution-quarter">
											<a href="/empire-connect">
												<div class="solution-div">
													<div class="quarter-img esc"></div>
													<!-- <img src="/wp-content/uploads/2017/08/office-girl-skype.jpg" alt="Solutions"> -->
													<h3>Empire Connect</h3>
													<p>Bring your existing voice solution to Skype for Business.</p>
												</div>
											</a>
										</div>

										<div class="col-md-6 solution-quarter">
											<a href="/professional-services">
												<div class="solution-div">
													<div class="quarter-img ps"></div>
													<!-- <img src="/wp-content/uploads/2017/08/professional-services-banner.jpg" alt="Solutions"> -->
													<h3>Professional Services</h3>
													<p>Turn process into results.</p>
												</div>
											</a>
										</div>

										<div class="col-md-6 solution-quarter">
											<a href="/empire-exchange">
												<div class="solution-div">
													<div class="quarter-img egx"></div>
													<!-- <img src="/wp-content/uploads/2017/06/empire-global-exchange-brighter-whiter.jpg" alt="Solutions"> -->
													<h3>Empire Exchange</h3>
													<p>Securely exchange networks across 11 countries.</p>
												</div>
											</a>
										</div>

							</div>

						</div>

			<!-- END MENU CONTAINER -->

			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- .wrapper-navbar end -->

	<div class="mobileMenu">
		<div class="container-fluid">
			<div class="row justify-content-between align-items-center">
				<h1><a href="/">Empire Solutions</a></h1>
				<i class="fa fa-bars" aria-hidden="true" id="activeBars" onclick="openOverlay()"></i>
			</div>
		</div>
	</div>

	<div class="overlayMenu" id="overMenu">
		<div class="x" id="x" onclick="closeOverlay()">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 content">
					<h2>Solutions</h2>
					<a href="/empire-storage">Empire Storage</a>
					<a href="/empire-connect">Empire Connect</a>
					<a href="/professional-services">Professional Services</a>
					<a href="/empire-exchange">Empire Exchange</a>
				</div>
				<div class="col-md-12 content">
					<h2>Company</h2>
					<a href="/about">About</a>
					<a href="/partners">Partners</a>
					<a href="/contact">Contact</a>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function openOverlay() {
			var overlay = document.getElementById('overMenu');
			overlay.classList.add('displayNow')
		}

		function closeOverlay() {
			var overlay = document.getElementById('overMenu');
			overlay.classList.remove('displayNow')
		}
	</script>
