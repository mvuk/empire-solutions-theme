
console.log("script runs");

var slider = document.getElementsByClassName('slider');
var solBtn = document.getElementById('solutions');
var esNav = document.getElementById('es-nav');
var navMenuArea = document.getElementById('nav-menu-area');

var navbarRight = document.getElementById('navbar-right');
var navX = '<a href="#" id="closeNav" onclick="closeTheMenu()"><i class="fa fa-times" aria-hidden="true"></i> Close Navigation</a>';
var navDefault = '<li><a href="#" id="solutions" onclick="openTheMenu()">Solutions</a></li><li><a href="/about">About</a></li><li><a href="/partners">Partners</a></li><li><a href="/contact">Contact</a></li>';


console.log(solBtn);
console.log(esNav);
console.log(navMenuArea);

// onclick solutions button
// solBtn.onclick = function()

function openTheMenu() {

console.log("btn is clicked");

  // make sure slider doesnt move, select the first instance of slider class
  slider[0].setAttribute("class","container-fluid slider slider-active");

  // add a class to ID "#es-nav"
  esNav.setAttribute("class","navbar navbar-toggleable-md es-nav es-nav-active");

  console.log("es-nav-active");
  // after a second, remove the class .inactive from #nav-menu-area

  navMenuArea.setAttribute("class","container nav-menu-area");

  //#navbar right remove the current items, replace with one link that has an X

  navbarRight.innerHTML = navX;

};


function closeTheMenu() {
  console.log('close the menu!')
  navbarRight.innerHTML = navDefault;

  slider[0].setAttribute("class","container-fluid slider");

  esNav.setAttribute("class","navbar navbar-toggleable-md es-nav");
  navMenuArea.setAttribute("class","container nav-menu-area inactive");
}
